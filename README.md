SimpleDiskEnv
=============

This is a simple python module to set up RADMC-3D models for disk + envelope 
systems. It was originally developed for the publication Agurto-Gangas et al., 2018, 
Astronomy & Astrophysics, Volume 623, 147 (DOI:10.1051/0004-6361/201833666).

The code aims to be well documented, thus it could be a good starting point for 
those who wants to learn how to craft input models for RADMC-3D.

The purpose / function of this module is summarized as:

1. Set up disk + envelope RADMC-3D models with independent dust opacities in 
   the two components.
    - Interstellar irradiation
    - Optional envelope density profiles (power law, Ulrich 1976, Tafalla+ 2004)
    - Optional cavity geometries

2. Visualize result dust temperature and density distribution and compute 
   theoretical visibilities (amplitude in UV space) from resulting images

3. Truncate model at arbitrary radius to analyse emission from individual 
   regions / components in multi-component models


Requirements:
------------

- Python 2.7 or Python 3.7
- matplotlib
- numpy
- [radmc3dPy](https://www.ast.cam.ac.uk/~juhasz/radmc3dPyDoc)
- [RADMC3D](http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d)
- Jupyter-notebook (optional)
- [galario (optional)](https://github.com/mtazzari/galario)

Installation:
------------

Download the repository in your browser or using the git clone utility. With the 
following command install it to the `~/.local/lib/python[version]/site-packages` 
directory. If you want to install is system-wide, then omit the --user flag (you will 
need superuser rights).

```bash
python setup.py install --user
```

Alternatively, you may add the code folder to your $PYTHONPATH:

```
$ export PYTHONPATH=$PYTHONPATH:/path/to/your/SimpleDiskEnv/directory
```
You can make this addition permanent by saving the export command to the 
~/.bashrc or ~/.profile files.

After completing this step, the module should be available in python:

```python
import SimpleDiskEnv
```

Contents:
---------
```
SimpleDiskEnv                Module containing the following files / functions:
   |
   |_ plots.py
   |  |
   |  |_ plotUV()            Computes visibility from RADMC-3D image output.
   |  |
   |  |_ plotModel()         Plots radmc3dData object density and temperature 
   |                         data in cylindrical coordinates. Displays multiple
   |                         dust components if present.
   |
   |_ truncate.py
   |  |
   |  |_ truncateModel()     Truncates instance of radmc3dData class (radmc3dPy)
   |  |                      at arbitrary radius.
   |  |
   |  |_ writeNewModel()     Writes radmc3dData instance to the current folder.
   |                         Used for writing the truncated model.
   |
   |_ create.py
      |
      |_ ISradField()        Calculates and writes out the Draine (1978) + Black 
      |                      (1994) angle averaged local interstellar radiation
      |                      field intensity.
      |_ simpleRadmc3Dmodel  Creates the input files for RADMC-3D. The denisty 
                             distribution is defined within this code.

examples                     Folder contaning jupyter notebooks, example 
   |                         parameter files and dust opacity files.
   |
   |_ problem_mspec.inp      Parameter file for disk + envelope system with 
   |                         different dust species in disk and envelope.
   |
   |_ problem_ulrich.inp     Parameter file for Ulrich (1976) rotationally  
   |                         flattened envelope. Minimal required parameter list.
   |
   |_ duskappa_biggrains.inp Dust opacity file from Miotello et al. (2014),
   |                         for water:carbon:silicate (6:3:1) composition and 
   |                         n(a) ~ a**(-3.5), amin=0.1 um, amax = 1 mm.
   |
   |_ dustkappa_osshenn_     Opacity file for the thin ice model coagulated 
   |  thin.inp               at n=10^5 for 10^6 years from Ossenkopf & Henning 
   |                         (1994).
   |
   |_ Ulrich_envelope_       Jupyter notebook for creating and Ulrich envelope
   |  ISRF.ipynb             and testing the effect of irradiation sources on 
   |                         the dust temperature.
   |
   |_ Class_I_multidust.ipynb Notebook example of Class I system (disk+envelope)
   |                          where the components have different dust optical
   |                          properties. Example for computing visibility 
   |                          plots. (same parameters as in problem_mspec.inp)
   |
   |_ Backwarming.ipynb      Notebook to illustrate the effect of backwarming.
                             It provides an example in using the model truncation
                             to analyse emission from different regions.

README.md                    This file.

LICENSE.md                   SimpleDiskEnv is licensed under GPLv2. The file 
                             contains the licensing information and usage 
                             conditions.
                             
```

Questions/problems to address:
------------------------------

### Different dust opacity in disk and envelope

The problem_params_mspec.inp contains the parameters for this setup.
Pay attention to the following parameters in the file:

```
dustkappa_ext = ['biggrains','osshenn_thinextra']
ngs = 2 
```

This gives the filenames and the number of dust species. The first opacity 
given in dustkappa_ext will be used for the disk, the second for the envelope.
These parameters will be used to write dustopac.inp.

If the model includes both disk and envelope, and you give 2 files in 
dustkappa_ext and set ngs to 2, then the dust_density.inp will contain the 
disk as the first dust species and the envelope as the second dust species.

See lines 299 -- 307 in SimpleDisk.py

For an example check out "Envelope test - Multiple dust species.ipynb".

### Envelope

Density structure of the envelope:

```python
# power-law envelope
rho = rho0 * (r/r0)^-2

# Tafalla (2002) envelope
rho = rho0 / (1.+ (r/r0)^2)

# where
r0 = cs / (32.*rho0 * G / 3 !pi)^1/2   # Keto & Caselli 2010
cs = sqrt(1.67 * k * T / mu / mp)
```

Note that `r0` may be set as a parameter.

Using some "typical" values:

In our envelope: T = 200 K (at the inner edge)
```
rho0 = 2.2e-15 --> r0 = 420 au
```

Cool, dense envelope: T=20 K
```
--> r0 = 132 au
```

Cool, not so dense: T=20 K
```
rho0 = 2.2e-18 (~1e6 /cm^3) --> r0 = 4201 au
```

### Is the external radiation field important?

We find that it is important in heating the outer 5 - 10 k au to about 10 - 15
K. The emission from this region due to the external heating contribute
~ 5 per cent to the emission above 150 micron, and ~15 per cent between 350 
and 600 micron. (see the wiki)

Acknowledgement:
---------------

The module relies on functionality provided by the radmc3dPy library (developed by 
Attila Juhász). The code for the Ulrich (1976) envelope model is adapted from 
the [HYPERION radiative transfer code](http://www.hyperion-rt.org/).
