#
# Truncates RADMC-3D model in spherical coordinate system at arbitrary 
# inner and outer edges (truncate_model). The resulting model can be 
# written to files using the write_new_env_model() function.
#
# Example: (in a folder containing a RADMC-3D model)
#
# import radmc3dPy
# import truncate_model
#
# data = radmc3dPy.analyze.readData(ddens=True,dtemp=True,binary=False)
# new = truncate_model.truncate_model(data, rtr_out_au=30.)
#
# truncate_model.write_new_env_model(new)
#
# import SimpleDisk
# SimpleDisk.plotmodels(new,rlim=[0,35],zlim=[0,35])
#
# Written by Laszlo Szucs, 2018


import numpy as np
import copy

def truncateModel(data, rtr_in_au=0., rtr_out_au=30., use_spec=2):       
    '''
    Takes an instance of radmc3dData class and truncates it at the 
    given inner and outer radius. If more than one dust species 
    is defined in the radmc3dData instance, then choose between them.

    Parameters
    ----------

    data:        instance of radmc3dData class
    rtr_in_au:   inner truncation radius in au (default=0.0)
    rtr_out_au:  outer truncation radius in au (default=30.0)
    use_spec:    set the maximum index of dust species to keep,
                 if multiple species are present, i.e. if 2 
                 species are present and use_spec=2, then both
                 is kept, if use_spec=1, only the first species 
                 is kept.

    Returns
    -------
    
    Instance of radmc3dData class, with truncated desnity, temperature and 
    grid arrays. Other model content remains as the input model.

    To write the model to a folder, use the write_new_env_model() function.
    It can be readily used in SimpleDisk.plotmodels() function.
    '''             
    new = copy.deepcopy(data)
    au = 1.495978707e+13    # au in cm
    trout_au = rtr_out_au * au
    trin_au  = rtr_in_au  * au
    # Find the closest element in the radial array to rtrunc_au 
    idx_in  = (np.abs(data.grid.xi-trin_au)).argmin()
    idx_out = (np.abs(data.grid.xi-trout_au)).argmin() + 1
    # Update the model
    new.grid.x  = new.grid.x[idx_in:idx_out]
    new.grid.xi = new.grid.xi[idx_in:idx_out+1]
    new.grid.nx = len(new.grid.x)
    new.grid.nxi = len(new.grid.xi)

    if np.shape(data.dusttemp)[3] > 1:
       spec = range(0,use_spec)
    else:
       spec = [0]

    new.dusttemp = new.dusttemp[idx_in:idx_out,:,:,spec]
    new.rhodust = new.rhodust[idx_in:idx_out:,:,:,spec]

    return new


def writeNewModel(data, nspec=None):
    '''
    Write a radmc3dData class object to grid (amr_grid.inp), density 
    (dust_density.inp) and temperature (dust_temperature.dat) RADMC-3D 
    files.

    A use case for this code is to write the truncated models to files.
    Note that the above mentioned files will be overwritten in the folder,
    if they are present.

    Parameters
    ----------
 
    data:        instance of radmc3dData class
    nspec:       number of species to be written, if None use all
    '''                      
    DIMP = data.grid.nz               
    DIMT = data.grid.ny                                          
    DIMR = data.grid.nx                                                 
               
    if nspec is None:
       NSPEC = np.shape(data.dusttemp)[3] 
    else:
       NSPEC = nspec

    gastodust = 100.                                                    
                
# Write amr_grid.binp
#
    fout = open('amr_grid.inp','w')
    fout.write('{:8n}\n'.format(1))             # iformat
    fout.write('{:8n}\n'.format(0))             # AMR grid style (0=regular grid, no AMR)
    fout.write('{:8n}\n'.format(100))           # Coordinate system (100 spherical)
    fout.write('{:8n}\n'.format(0))                     # gridinfo
    fout.write('{:8n}  {:8n}  {:8n}\n'.format(data.grid.act_dim[0],    # Include x,y,z coordinate
                                              data.grid.act_dim[1], 
                                              data.grid.act_dim[2]))
    fout.write('{:8n}  {:8n}  {:8n}\n'.format(DIMR,DIMT,DIMP)) # grid size
    for ix in range(DIMR+1):      # write cell interface coordinates (therefore the +1)
        fout.write('{:11.6E}\n'.format(data.grid.xi[ix]))
    for it in range(DIMT+1):      # write cell interface coordinates (therefore the +1)
        fout.write('{:11.6E}\n'.format(data.grid.yi[it])) 
    for ip in range(DIMP+1):      # write cell interface coordinates (therefore the +1)
        fout.write('{:11.6E}\n'.format(data.grid.zi[ip]))
    fout.close()


# Write the density file
#
    gastodust = 100.
    fout = open('dust_density.inp','w')
    fout.write('{:8n}\n'.format(1))                      # Format number

    fout.write('{:8n}\n'.format(DIMR*DIMT*DIMP))         # Nr of cells
    fout.write('{:8n}\n'.format(NSPEC))                  # Nr of dust species
    for idust in range(NSPEC):
        for ip in range(DIMP):
            for it in range(DIMT):
                for ir in range(DIMR):
                    fout.write('{:11.4E}\n'.format(data.rhodust[ir,it,ip,idust]))
    fout.close()

# Write gas/dust temperature
#
    gastodust = 100.
    fout = open('dust_temperature.dat','w')
    fout.write('{:8n}\n'.format(1))                      # Format number

    fout.write('{:8n}\n'.format(DIMR*DIMT*DIMP))         # Nr of cells
    fout.write('{:8n}\n'.format(NSPEC))                  # Nr of dust species
    for idust in range(NSPEC):
        for ip in range(DIMP):
            for it in range(DIMT):
                for ir in range(DIMR):
                    fout.write('{:11.4E}\n'.format(data.dusttemp[ir,it,ip,idust]))
    fout.close()

