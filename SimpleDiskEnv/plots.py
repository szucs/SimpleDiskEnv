# import 3rd party libraries
import os
import matplotlib.pyplot as plt
import numpy as np
import copy

import radmc3dPy
from radmc3dPy import natconst
from radmc3dPy import analyze

try:
    import galario
except:
    print ('Galario not found, defaulting to radmc3dPy visibility.')
    pass

def plotUV(image=None, fname='image.out', dpc=1.0, uv=None, use_galario=False, 
           blr=[1,1000], nbl=100, label='', xlim=[0,100], ylim=[0,160], 
           xlog=False, ylog=False, oplot=False, show=True, wle=2000, 
           bin_data=False):
   '''
   Computes and plots the visibility amplitude sqrt(Re**2 + Im**2) of a RADMC3D 
   image using the radmc3dPy or Galario library.
   
   Important note: The radmc3dPy built in tool for computing visibility gives 
   a reasonable first order approximation. For publication quality figures 
   always use the Galario mode!
   
   Also note that if realistic uv distances are specified in uv, then the code 
   plots the un-binned data. To plot binned data, use the UVPLOT python library.
   
   Parameters
   ----------
     image  --  radmc3dPy.image.radmc3dImage type object (optional)
     fname  --  string giving the RADMC-3D image output name. If image parameter
                not specified then read image with this name.
     dpc    --  distance to the object in units of parsec (default 1.0 parsec)
     uv     --  numpy array specifying the u and v positions in units of meter
                (optional)
     use_galario -- if True then use the optimised Galario code to compute the 
                    visibility. Must be False if Galario is not installed.
     blr    --  two element list of array defining the baseline range in meters
                (used if uv parameter is not set)
     nbl    --  number of baselines (used if uv parameter is not set)
     label  --  string labelling the plotted visibility amplitude
     xlim   --  plotting range on x axis (in units of kilo-lambda)
     ylim   --  plotting range on y axis (in units of mJy)
     xlog   --  Bool, if True plot x axis logarithmically
     ylog   --  Bool, if True plot y axis logarithmically
     oplot  --  Bool, if True plot visibility amplitude curve on existing figure
     show   --  Bool, if True display figure
     bin_data -- plot binned data
     wle    -- if binned data is plotted, then wle gives the with of the bins in 
               units of wavelength (default 2000)
   '''
   
   au = natconst.au   # astronomical unit in cgs
   pc = natconst.pc   # parsec in cgs

   if not image:
      image = radmc3dPy.image.readImage(fname=fname)
      
   if uv is None:
      uv = np.random.rand(nbl,2) * (blr[1]-blr[0]) + blr[0]
      uv.sort(axis=0)
      use_real_uv = False
   else:
      use_real_uv = True
   
   bl = np.sqrt(uv[:,0]**2 + uv[:,1]**2)
   pa = np.arctan(bl*100. / (dpc*pc)) * 180./np.pi

   #print (pa)

   if use_galario:
      dxy = np.arctan( image.sizepix_x / (dpc*pc) )
#      dxy = image.sizepix_x / radmc3dPy.natconst.au / dpc / 3600. * galario.deg
      u = uv[:,0] / ( image.wav[-1] * 1.0e-6 )
      v = uv[:,1] / ( image.wav[-1] * 1.0e-6 )
      
      imJyppix = image.imageJyppix[:,:,-1] / dpc**2
      
      uvdata = galario.double.sampleImage(imJyppix, dxy, u, v)
      baseline = np.sqrt(u**2 + v**2)
      ampl_mJy = np.sqrt(uvdata.real**2 + uvdata.imag**2) * 1.0e3

   else:
      uvdata   = image.getVisibility(bl=bl,pa=pa,dpc=dpc)
      baseline = np.sqrt(uvdata['u']**2 + uvdata['v']**2)
      ampl_mJy = uvdata['amp']/1.0e-23 * 1.0e3

   # Bin data
   if bin_data and use_real_uv:
      nbin = (baseline.max() - baseline.min()) // wle
      bl_bin = np.linspace(baseline.min(), baseline.max(), nbin)
      ampl_mean = np.zeros_like(bl_bin)
      ampl_std  = np.zeros_like(bl_bin)
   
      bin_indx  = np.searchsorted(bl_bin, baseline)
   
      for i in range(int(nbin)):
         ampl_mean[i] = ampl_mJy[np.where(bin_indx == i)].mean()
         ampl_std[i] = np.std(ampl_mJy[np.where(bin_indx == i)])

   # Create the figure
   if oplot:
      
      if bin_data and use_real_uv:
          plt.errorbar(bl_bin/1000., ampl_mean, yerr=ampl_std, fmt='o')
      else:
          plt.plot(baseline/1000., ampl_mJy, label=label)
      
      plt.legend()
   else:
      fig = plt.figure()
      ax1 = fig.add_subplot(111)

      if bin_data and use_real_uv:
          plt.errorbar(bl_bin/1000., ampl_mean, yerr=ampl_std, fmt='o')
      else:
          ax1.plot(baseline/1000., ampl_mJy, label=label)
          
      ax1.set_xlim(xlim)
      ax1.set_ylim(ylim)
      # set axis scaling
      if xlog:
         ax1.set_xscale('log')
      if ylog:
         ax1.set_yscale('log')

      if label != '':
         ax1.legend()
      ax1.set_xlabel('uv distance [k$\lambda$]')
      ax1.set_ylabel('amplitude [mJy]')

      # add the second y axis with linear resolution
      ax1Xs = ax1.get_xticks()
      ax2Xs = [] 
      for val in ax1Xs:
          if val > 0.0:
             ax2Xs.append("{0:.2f}".format(dpc * pc / (val * 1000.) / au))
          else:
             ax2Xs.append("Inf")
      ax2 = ax1.twiny()
      ax2.set_xticks(ax1Xs)
      ax2.set_xbound(ax1.get_xbound())
      ax2.set_xticklabels(ax2Xs)
      ax2.set_xlabel('linear resolution [au]')

      if show:
         plt.show()

def plotModel(data, ddens=True, dtemp=True, rhomin=-19, rhomax=-10,
              Tmin=0., Tmax=200., xlog=False, ylog=False, cmap='Spectral_r',
              rlim=[0,200],zlim=[0,200], phislice=0, title=None, show=True,
              comps=['disk','env'],savefig=False,contour=True, ncont=10):
   '''
   Takes a radmc3dPy model object and plots its density and temperature 
   distributions in cylindrical coordinates.
    
   Note that if there are more than one dust species in the model, then all
   components will be plotted.
   
   '''

   au = natconst.au   # astronomical unit in cgs
   pc = natconst.pc   # parsec in cgs
   
   # Number of grain species in the model
   ngs = data.rhodust.shape[3]
   isft = 0

   # Convert between spherical and cylindrical coordinates
   rr, th = np.meshgrid(data.grid.x, data.grid.y)

   zz   = rr * np.cos(th)
   rcyl = rr * np.sin(th)

   phideg = np.double(phislice) / np.double(data.grid.nz) * 360. # rad to deg

   # Decide which components to show
   if ('env' not in comps) and (ngs > 1):
      ngs  = 1
      isft = 0
   if ('disk' not in comps) and (ngs > 1):
      ngs  = 1
      isft = 1


   # Plot the density
   if ddens:
      f, ax = plt.subplots(ngs, 1, figsize=(10,8*ngs))
      if ngs == 1:
        ax = [ax]

      for i in range(ngs):
        plt.subplot(ax[i])
        rhodust = copy.deepcopy(data.rhodust[:,:,phislice,i+isft])
        if np.nanmin(rhodust) == 0.0:
            rhodust[ rhodust == 0.0 ] = np.NaN
        PltDens = plt.pcolormesh(rcyl/au, zz/au, np.log10(rhodust.T), 
                                 vmin=rhomin, vmax=rhomax, cmap=cmap)
        if contour:
           ConDens = plt.contour(rcyl/au, zz/au, 
                                 np.log10(rhodust).T.clip(rhomin, rhomax), 
                                 ncont, colors='k', linestyles='solid')
           plt.clabel(ConDens, inline=1, fontsize='small')
        plt.xlabel('r [AU]')
        plt.ylabel(r'$\pi/2-\theta$')
        if xlog:
            plt.xscale('log')
        if ylog:
            plt.yscale('log')
        ax[i].set_xlim(rlim)
        ax[i].set_ylim(zlim)
        ax[i].set_aspect('equal')
        ax[i].set_xlabel('R [AU]')
        ax[i].set_ylabel('Z [AU]')
        if title:
            ax[i].set_title(title)
        if (ngs > 1) and (i+isft == 0):
            ax[i].annotate('Disk component', (0.6, 0.9), 
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        if (ngs > 1) and (i+isft == 1):
            ax[i].annotate('Envelope component', (0.6, 0.9), 
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        if (data.grid.nz > 1):
            ax[i].annotate('$\phi = ${:.1f} deg'.format(phideg), (0.1, 0.9), 
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        cbD = plt.colorbar(PltDens)
        cbD.set_label(r'$\log_{10}(\rho_\mathrm{d})$ [g cm$^{-3}$]')

   # Plot the temperature
   if dtemp:
      f, ax = plt.subplots(ngs, 1, figsize=(10,8*ngs))
      if ngs == 1:
        ax = [ax]
      for i in range(ngs):
        plt.subplot(ax[i])
        Tdust = data.dusttemp[:,:,phislice,i].T.clip(Tmin, Tmax)
        PltTemp = plt.pcolormesh(rcyl/au, zz/au, Tdust, vmin=Tmin, vmax=Tmax, 
                                 cmap=cmap)
        if contour:
           ConTemp = plt.contour(rcyl/au, zz/au, Tdust, ncont, 
                                 vmin=Tmin, vmax=Tmax,
                                 colors='k', linestyles='solid')
           plt.clabel(ConTemp, inline=1, fontsize='small')
        plt.xlabel('r [AU]')
        plt.ylabel(r'$\pi/2-\theta$')
        if xlog:
            plt.xscale('log')
        ax[i].set_xlim(rlim)
        ax[i].set_ylim(zlim)
        ax[i].set_aspect('equal')
        ax[i].set_xlabel('R [AU]')
        ax[i].set_ylabel('Z [AU]')
        if title:
            ax[i].set_title(title)
        if ngs > 1 and i == 0:
            ax[i].annotate('Disk component', (0.6, 0.9), 
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        if ngs > 1 and i == 1:
            ax[i].annotate('Envelope component', (0.6, 0.9), 
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        if (data.grid.nz > 1):
            ax[i].annotate('$\phi = ${:.1f} deg'.format(phideg), (0.1, 0.9),  
                           xycoords='axes fraction', 
                           bbox={'facecolor':'white', 'alpha':0.9, 'pad':5},
                           fontsize=14)
        cbT = plt.colorbar(PltTemp)
        cbT.set_label(r'T [K]') 

   if savefig:
       if type(savefig) == str:
          figfile = savefig
       else:
          figfile = 'radmc3dmodel.png'
       plt.savefig(figfile,ext='png')
   if show:
        plt.show()
