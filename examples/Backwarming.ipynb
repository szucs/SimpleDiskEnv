{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Backwarming example\n",
    "\n",
    "If the envelope is optically thick then it might act as a thermal cavity. The temperature in the cavity can not fall below the cavity wall temperature (approximately the envelope temperature at optical depth of unity). The cavity temperature in turns will set the lowest temperature in the disk itself, independent of the disk optical depth (in steady state models). This effect was in fact recognized by Butner et al. (1994) and named \"backwarming\".\n",
    "\n",
    "Generally, this means that the disk component can not be modelled independently from the envelope (as e.g. in Miotello et al. 2014). The magnitude of the effect depends on the chosen envelope model, envelope dust opacity and envelope mass.\n",
    "\n",
    "We show the effect by comparing the visibility of a disk without envelope and the same disk surrounded by envelope.\n",
    "\n",
    "It is not trivial to separate the disk emission from the envelope emission in the disk+envelope models. Our approach is to truncate the full model (which is used in the dust temperature computation) at the disk radii and calculate the visibility emerging from the remaining (r $\\leq$ rdisk) region."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import modules\n",
    "import os               # we use this for folder related commands\n",
    "import shutil           # we use this for file copying\n",
    "\n",
    "import radmc3dPy\n",
    "import SimpleDiskEnv\n",
    "\n",
    "# In line plotting\n",
    "%matplotlib inline\n",
    "\n",
    "# Function to create working directory and to copy opacity files\n",
    "def makedir(model,WORK_DIR):\n",
    "    if not os.path.isdir(WORK_DIR+'/'+model):\n",
    "        os.makedirs(WORK_DIR+'/'+model)\n",
    "    os.chdir(WORK_DIR+'/'+model)\n",
    "    # Copy required files\n",
    "    file_source = [WORK_DIR+'/dustkappa_biggrains.inp',\n",
    "                   WORK_DIR+'/dustkappa_osshenn_thinextra.inp']\n",
    "    file_dest = WORK_DIR+'/'+model\n",
    "    for i in range(len(file_source)):\n",
    "        shutil.copy2(file_source[i],file_dest)\n",
    "        \n",
    "# Define root work directory\n",
    "WORK_DIR = os.getcwd()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create example working directory in the local folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "makedir('backwarming/disk',WORK_DIR)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Write parameter file\n",
    "\n",
    "The parameter values are those found for Elisa29 in Miotello et al. (Astronomy & Astrophysics, Volume 567, A32). The authors used a different aproach for the disk model, therefore differences are expected.\n",
    "\n",
    "Notice the *dustkappa_ext* and *ngs* parameters! The former is a list of dust opacity files. The later decides how many elements of the list is to be used. Currently the largest number is 2: first component sets the opacity in the disk, second component sets the opacity in the envelope. The *ngs* parameters is useful when the list has more than one element, but you want to use only the first element for both model components."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file problem_backwarming.inp\n",
    "#\n",
    "# Block: Radiation sources\n",
    "#\n",
    "mstar       = [2.5*ms]         # Mass of the star(s)\n",
    "pstar       = [0.0, 0.0, 0.0]  # Position of the star(s) (cartesian coordinates)\n",
    "rstar       = [5.9*rs]         # Radius of the star(s)\n",
    "tstar       = [4786.0]         # Effective temperature of the star(s) [K]\n",
    "#\n",
    "# Block: Grid parameters\n",
    "#\n",
    "crd_sys     = 'sph'            # Coordinate system used (car/cyl)\n",
    "xbound      = [1.*au,50*au,5e4*au] # Boundaries for the x grid\n",
    "nx          = [60,100]         # Number of grid points in the first dimension\n",
    "ybound      = [0.,pi/2.]       # Boundaries for the y grid\n",
    "ny          = 80               # Number of grid points in the second dimension\n",
    "zbound      = [0.,2.0*pi]      # Boundaries for the z grid\n",
    "nz          = 0                # Number of grid points in the third dimension\n",
    "#\n",
    "# Block: Wavelength grid parameters\n",
    "#\n",
    "wbound      = [0.1,7.,25.,1e4] # Boundaries for the wavelength grid\n",
    "nw          = [50,150,100]     # Number of points in the wavelength grid\n",
    "#\n",
    "# Block: Dust opacity\n",
    "#\n",
    "dustkappa_ext = ['biggrains','osshenn_thinextra']  # Opacity file extension: dustkappa_[dustkappa_ext].inp\n",
    "ngs         = 2                # Number of grain sizes\n",
    "#\n",
    "# Block: Code parameters\n",
    "#\n",
    "istar_sphere         = 1         # 1 - finite size star, 0 - point-like star\n",
    "nphot                = long(1e6) # Nr of photons for the thermal Monte Carlo\n",
    "modified_random_walk = 1         # Use the modified random walk method to improve speed?\n",
    "scattering_mode_max  = 0         # 0 - no scattering, 1 - isotropic scattering, 2 - anisotropic scattering\n",
    "#\n",
    "# Block: Model parameters\n",
    "#\n",
    "bgdens      = 1e-40            # Background density (g/cm^3)\n",
    "dusttogas   = 0.01             # Dust-to-gas mass ratio\n",
    "#\n",
    "# Disk\n",
    "#\n",
    "mdisk       = 0.01*ms          # Mass of the disk\n",
    "rin         = 1.0*au           # Inner radius of the disk\n",
    "rdisk       = 50.0*au          # Outer radius of the disk\n",
    "hrdisk      = 0.1              # Ratio of the pressure scale height over radius at hrpivot\n",
    "hrpivot     = 50.0*au          # Reference radius at which Hp/R is taken\n",
    "plh         = 2.0/7.0          # Flaring index\n",
    "plsig1      = -1.0             # Power exponent of the surface density distribution as a function of radius\n",
    "plsig2      = -40.0            # Power law exponent at r > rdisk (abrubt cutoff at rdisk is not realistic)\n",
    "#\n",
    "# Envelope\n",
    "#\n",
    "modeEnv     = 'Ulrich1976'     # Choose envelope model, options: ['Ulrich1976','Tafalla2004','powerlaw']\n",
    "rho0Env     = 4e-20            # Characteristic gas+dust volume density in g/cm3\n",
    "r0Env       = 300.0*au         # Characteristic radius in cm\n",
    "rTrunEnv    = 50.0*au          # Truncation radius in cm (inner envelope edge)\n",
    "redFactEnv  = 1e-2             # Density is reduced by this factor if r < rTrunEnv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Disk only model\n",
    "\n",
    "Compute the disk only model and image at 1300 micron."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Write input files for RADMC-3D\n",
    "SimpleDiskEnv.simpleRadmc3Dmodel(disk=True, cavity=False, envelope=False,\n",
    "                                 paramfile='problem_backwarming.inp')\n",
    "\n",
    "# Run the model using multi-threaded mode (4 cores)\n",
    "print (\"\\nRADMC-3D runtime:\")\n",
    "!time radmc3d mctherm setthreads 4 > radmc3d.log\n",
    "\n",
    "# Compute image, resolution: 200px * 200px, 3000 au * 3000 au, inclination 60 deg, \n",
    "# wavelength 1300 micron\n",
    "print (\"\\nImage calculation time:\")\n",
    "!time radmc3d image npix 200 lambda 1300 incl 60 sizeau 3000 > image.log\n",
    "\n",
    "im_disk = radmc3dPy.image.readImage(fname='image.out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Disk + envelope model\n",
    "\n",
    "Compute temperature with both components."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Write input files for RADMC-3D\n",
    "SimpleDiskEnv.simpleRadmc3Dmodel(disk=True, cavity=False, envelope=True,\n",
    "                                 paramfile='problem_backwarming.inp')\n",
    "\n",
    "# Run the model using multi-threaded mode (4 cores)\n",
    "print (\"\\nRADMC-3D runtime:\")\n",
    "!time radmc3d mctherm setthreads 4 > radmc3d.log\n",
    "\n",
    "# Compute image, resolution: 200px * 200px, 3000 au * 3000 au, inclination 60 deg, \n",
    "# wavelength 1300 micron\n",
    "print (\"\\nImage calculation time:\")\n",
    "!time radmc3d image npix 200 lambda 1300 incl 60 sizeau 3000 > image.log\n",
    "\n",
    "im_disk_env = radmc3dPy.image.readImage(fname='image.out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Truncated model\n",
    "\n",
    "Read in the model data using radmc3dPy and truncate the model at 50 au (disk radius). Write out the truncated model and compute image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = radmc3dPy.analyze.readData(ddens=True,dtemp=True,binary=False)\n",
    "\n",
    "trunc = SimpleDiskEnv.truncate.truncateModel(data, rtr_out_au=50.)\n",
    "\n",
    "SimpleDiskEnv.truncate.writeNewModel(trunc) \n",
    "\n",
    "# Compute image, resolution: 200px * 200px, 3000 au * 3000 au, inclination 60 deg, \n",
    "# wavelength 1300 micron\n",
    "print (\"\\nImage calculation time:\")\n",
    "!time radmc3d image npix 200 lambda 1300 incl 60 sizeau 3000 > image.log\n",
    "\n",
    "im_trunc = radmc3dPy.image.readImage(fname='image.out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visibilities\n",
    "\n",
    "The comparision shows that the bare disk constitutes less emission than the disk in the embaded case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Plot visibility\n",
    "blr = [1,200]\n",
    "nbl = 40\n",
    "ylim=[0,400]\n",
    "dpc = 125.\n",
    "\n",
    "# First call sets the axes, second over-plots data with label\n",
    "SimpleDiskEnv.plotUV(image=im_disk_env,dpc=dpc,blr=blr,nbl=nbl,ylim=ylim,show=False)\n",
    "SimpleDiskEnv.plotUV(image=im_disk_env,label='Disk + Envelope',dpc=dpc,blr=blr,\n",
    "                     nbl=nbl,ylim=ylim,oplot=True)\n",
    "\n",
    "# Add disk only visibility\n",
    "SimpleDiskEnv.plotUV(image=im_disk,label='Disk',dpc=dpc,blr=blr,\n",
    "                     nbl=nbl,ylim=ylim,oplot=True)\n",
    "\n",
    "# Add truncated model visibilty\n",
    "SimpleDiskEnv.plotUV(image=im_trunc,label='Disk + Envelope (truncated)',dpc=dpc,blr=blr,\n",
    "                     nbl=nbl,ylim=ylim,oplot=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
