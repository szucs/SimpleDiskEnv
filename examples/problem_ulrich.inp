#
# Block: Radiation sources
#
mstar       = [2.5*ms]         # Mass of the star(s)
pstar       = [0.0, 0.0, 0.0]  # Position of the star(s) (cartesian coordinates)
rstar       = [5.9*rs]         # Radius of the star(s)
tstar       = [4786.0]         # Effective temperature of the star(s) [K]
#
# Block: Grid parameters
#
crd_sys     = 'sph'            # Coordinate system used (car/cyl)
xbound      = [1.*au,1.05*au,5e4*au] # Boundaries for the x grid
nx          = [30,100]         # Number of grid points in the first dimension
ybound      = [0.,pi/2.]       # Boundaries for the y grid
ny          = 80               # Number of grid points in the second dimension
zbound      = [0.,2.0*pi]      # Boundaries for the z grid
nz          = 0                # Number of grid points in the third dimension
#
# Block: Wavelength grid parameters
#
wbound      = [0.1,7.,25.,1e4] # Boundaries for the wavelength grid
nw          = [50,150,100]     # Number of points in the wavelength grid
#
# Block: Dust opacity
#
dustkappa_ext = ['osshenn_thinextra']  # Opacity file extension: dustkappa_[dustkappa_ext].inp
ngs         = 1                # Number of grain sizes
#
# Block: Code parameters
#
istar_sphere         = 1         # 1 - finite size star, 0 - point-like star
nphot                = long(1e6) # Nr of photons for the thermal Monte Carlo
modified_random_walk = 1         # Use the modified random walk method to improve speed?
scattering_mode_max  = 0         # 0 - no scattering, 1 - isotropic scattering, 2 - anisotropic scattering
#
# Block: Model parameters
#
bgdens      = 1e-40            # Background density (g/cm^3)
dusttogas   = 0.01             # Dust-to-gas mass ratio
modeEnv     = 'Ulrich1976'     # Choose envelope model, options: ['Ulrich1976','Tafalla2004','powerlaw']
rho0Env     = 4e-20            # Characteristic gas+dust volume density in g/cm3
r0Env       = 300.0*au         # Characteristic radius in cm
rTrunEnv    = 30.0*au          # Truncation radius in cm (inner envelope edge)
redFactEnv  = 1e-2             # Density is reduced by this factor if r < rTrunEnv