#!/usr/bin/env python

from setuptools import setup

try:
   import galario
   print ('galario found in PYTHONPATH')
except ImportError:
   print ('WARN: install galario manually (optional): https://github.com/mtazzari/galario')

setup(name='SimpleDiskEnv',
      version='0.1.1',
      install_requires=['radmc3dPy >= 0.30.2', 'numpy', 'matplotlib'],
      extras_require={'galario':['galario']},
      dependency_links=['https://www.ast.cam.ac.uk/~juhasz/radmc3dPyDoc/_downloads/radmc3dPy-0.30.2.tar.gz'],
      provides=['SimpleDiskEnv'],
      description='RADMC3D based Class 0/I/II protostar model creation tool.',
      author='Laszlo Szucs',
      author_email='laszlo.szucs@mpe.mpg.de',
      license='GPLv2',
      url='https://gitlab.mpcdf.mpg.de/szucs/SimpleDiskEnv',
      packages=['SimpleDiskEnv'],
      package_data={'SimpleDiskEnv':['lnk_files/*.lnk']},
     )
